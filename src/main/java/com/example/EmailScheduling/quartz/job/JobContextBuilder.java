package com.example.EmailScheduling.quartz.job;

import com.example.EmailScheduling.exсeptions.CustomSchedulerException;
import com.example.EmailScheduling.model.EmailModel;
import org.quartz.*;

import java.util.UUID;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

public class JobContextBuilder {

    public static void buildJobContext
            (final EmailModel emailModel, final Scheduler scheduler) throws CustomSchedulerException {
        final JobDetail jobDetails = buildJobDetail(emailModel);
        final SimpleTrigger trigger = buildTrigger(jobDetails);

        try {
            scheduler.scheduleJob(jobDetails, trigger);
        } catch (SchedulerException exception) {
            throw new CustomSchedulerException(exception.getMessage());
        }
    }

    private static JobDetail buildJobDetail(final EmailModel emailModel) {
        final JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("email", emailModel.getEmail());

        return JobBuilder.newJob(EmailJob.class)
                .withIdentity(UUID.randomUUID().toString(), "email")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private static SimpleTrigger buildTrigger(final JobDetail jobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName())
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(10)
                        .repeatForever())
                .build();
    }
}
