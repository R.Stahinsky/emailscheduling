package com.example.EmailScheduling.quartz.job;

import com.example.EmailScheduling.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

@RequiredArgsConstructor
public class EmailJob extends QuartzJobBean {

    private final EmailService emailService;

    @Override
    protected void executeInternal(final JobExecutionContext context) {
        emailService.sendEmail(context.getJobDetail().getJobDataMap().get("email").toString());
    }
}
