package com.example.EmailScheduling.exсeptions;

import org.quartz.SchedulerException;

public class CustomSchedulerException extends SchedulerException {

    public CustomSchedulerException(final String message) {
        super(message);
    }
}
