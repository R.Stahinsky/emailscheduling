package com.example.EmailScheduling.model;

import lombok.*;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class EmailModel {

    private UUID id;
    private String email;
    private Instant lastSendAt;
}
