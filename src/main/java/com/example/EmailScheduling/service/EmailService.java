package com.example.EmailScheduling.service;

import com.example.EmailScheduling.model.EmailModel;

import java.util.List;

public interface EmailService {

    void sendEmail(final String toEmail);

    EmailModel createEmail(final EmailModel emailModel);

    List<EmailModel> getAll();
}
