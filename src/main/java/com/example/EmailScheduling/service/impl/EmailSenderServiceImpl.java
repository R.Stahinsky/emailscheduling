package com.example.EmailScheduling.service.impl;

import com.example.EmailScheduling.entity.EmailEntity;
import com.example.EmailScheduling.mapper.EmailMapper;
import com.example.EmailScheduling.model.EmailModel;
import com.example.EmailScheduling.repository.EmailRepository;
import com.example.EmailScheduling.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.EmailScheduling.mapper.EmailMapper.mapToEntity;
import static com.example.EmailScheduling.util.constants.Messages.TEST_MESSAGE;
import static com.example.EmailScheduling.util.constants.Messages.TEST_SUBJECT;

@Service
@EnableScheduling
@RequiredArgsConstructor
public class EmailSenderServiceImpl implements EmailService {

    private final JavaMailSender mailSender;

    private final EmailRepository emailRepository;

    @Override
    @Transactional
    public void sendEmail(final String toEmail) {
        final SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(toEmail);
        message.setText(TEST_MESSAGE);
        message.setSubject(TEST_SUBJECT);

        final EmailEntity sentEmail = emailRepository.findByEmail(toEmail);
        sentEmail.setLastSendAt(Instant.now());

        mailSender.send(message);
        emailRepository.saveAndFlush(sentEmail);
    }

    @Override
    @Transactional
    public EmailModel createEmail(final EmailModel emailModel) {
        return EmailMapper.mapToModel(emailRepository.saveAndFlush(mapToEntity(emailModel)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmailModel> getAll() {
        return emailRepository.findAll().stream()
                .map(EmailMapper::mapToModel)
                .collect(Collectors.toList());
    }
}
