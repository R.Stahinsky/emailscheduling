package com.example.EmailScheduling.controller;

import com.example.EmailScheduling.exсeptions.CustomSchedulerException;
import com.example.EmailScheduling.model.EmailModel;
import com.example.EmailScheduling.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.quartz.Scheduler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.example.EmailScheduling.quartz.job.JobContextBuilder.buildJobContext;

@RestController
@RequestMapping("/emails")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;

    private final Scheduler scheduler;

    @GetMapping
    public ResponseEntity<List<EmailModel>> getAll() {
        return new ResponseEntity<>(emailService.getAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EmailModel> createEmail
            (@Valid @RequestBody final EmailModel emailModel) throws CustomSchedulerException {
        buildJobContext(emailModel, scheduler);
        return new ResponseEntity<>(emailService.createEmail(emailModel), HttpStatus.CREATED);
    }
}
