package com.example.EmailScheduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailSchedulingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailSchedulingApplication.class, args);
	}
}
