package com.example.EmailScheduling.entity;

import com.example.EmailScheduling.util.annotation.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email")
public class EmailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @Email
    @Column(name = "email_address", nullable = false, unique = true)
    private String email;

    @Column(name = "last_send_at")
    private Instant lastSendAt;
}
