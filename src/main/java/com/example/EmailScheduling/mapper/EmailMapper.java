package com.example.EmailScheduling.mapper;

import com.example.EmailScheduling.entity.EmailEntity;
import com.example.EmailScheduling.model.EmailModel;

import java.util.List;
import java.util.stream.Collectors;

public class EmailMapper {

    public static EmailModel mapToModel(final EmailEntity emailEntity) {
        return EmailModel.builder()
                .id(emailEntity.getId())
                .email(emailEntity.getEmail())
                .lastSendAt(emailEntity.getLastSendAt())
                .build();
    }

    public static EmailEntity mapToEntity(final EmailModel emailModel) {
        return EmailEntity.builder()
                .id(emailModel.getId())
                .email(emailModel.getEmail())
                .lastSendAt(emailModel.getLastSendAt())
                .build();
    }
}
