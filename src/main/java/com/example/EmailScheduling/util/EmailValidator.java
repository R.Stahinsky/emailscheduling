package com.example.EmailScheduling.util;

import com.example.EmailScheduling.util.annotation.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<Email, String> {

    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        final var matcher = EMAIL_PATTERN.matcher(value);
        return matcher.matches();
    }
}
