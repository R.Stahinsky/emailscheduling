package com.example.EmailScheduling.util.constants;

public class Messages {

    public static final String INVALID_EMAIL_MESSAGE = "Invalid email";
    public static final String TEST_MESSAGE = "This is a test message from quartz";
    public static final String TEST_SUBJECT = "Test subject";
}
