CREATE TABLE IF NOT EXISTS public.email
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    email_address text NOT NULL UNIQUE,
    last_send_at timestamp
);

ALTER TABLE public.email
    OWNER to postgres;
